<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
Route::get('/test2', 'ExampleController@test');


$router->get('/', function () use ($router) {
    return $router->app->version()."FROM CONSUMER";
});

/* $router->get('customers', [
    'as' => 'customers', 'uses' => 'CustomerController@customers'
]); */

$router->get('test', [
    'as' => 'test', 'uses' => 'PatronbaseController@test'
]);

Route::group([
    'middleware' => 'apiKey',
    'prefix' => '{client}'
], function () {
    // Route::get('/payment_allocations_by_date', 'PaymentAllocationsController@paymentAllocationsByDate');
    // Route::get('/test', 'PatronbaseController@test');
    // Route::get('/payment_allocations_from_date', 'PaymentAllocationsController@paymentAllocationsFromDate');
    // Route::post('/payment_allocations_from_date', 'PaymentAllocationsController@paymentAllocationsFromDate');
});
