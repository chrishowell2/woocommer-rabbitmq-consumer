<?php

namespace App\Models\PatronBase;

use Illuminate\Database\Eloquent\Model;

class PatronEmail extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $guarded = [];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tblPatronEmail';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    // protected $connection = 'sql';

    /**
     * The primary key for the model.
     *
     * @var string|bool
     */
    protected $primaryKey = 'ID';

    /**
     * Get the patron from that the email belongs to.
     *
     * @return \Illuminate\Database]Eloquent\Relations\BelongsTo
     */
    public function patron()
    {
        return $this->belongsTo(Patron::class, 'PatronID', 'PatronID');
    }
}
