<?php

namespace App\Models\PatronBase;

use Illuminate\Database\Eloquent\Model;

class RecurringSaleScheme extends Model
{
    protected $fillable = [];

    protected $table = 'tblRecurringSaleScheme';

    protected $primaryKey = 'SchemeID';

    protected $keyType = 'string';

    public $timestamps = false;


}
