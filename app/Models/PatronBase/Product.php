<?php

namespace App\Models\PatronBase;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [];

    protected $table = 'tblProduct';

    protected $primaryKey = 'ProductID';

    protected $keyType = 'string';

    public $timestamps = false;


}
