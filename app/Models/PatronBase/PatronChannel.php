<?php

namespace App\Models\PatronBase;

use Illuminate\Database\Eloquent\Model;

class PatronChannel extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $guarded = [];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tblPatronChannel';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    // protected $connection = 'sql';

    /**
     * The primary key for the model.
     *
     * @var string|bool
     */
    protected $primaryKey = 'PatronChannelID';

    /**
     * Get the patron from that the channel belongs to.
     *
     * @return \Illuminate\Database]Eloquent\Relations\BelongsTo
     */
    public function patron()
    {
        return $this->belongsTo(Patron::class, 'PatronID', 'PatronID');
    }
}
