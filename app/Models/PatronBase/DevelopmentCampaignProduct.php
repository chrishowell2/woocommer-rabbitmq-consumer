<?php

namespace App\Models\PatronBase;

use Illuminate\Database\Eloquent\Model;

class DevelopmentCampaignProduct extends Model
{
    protected $fillable = [];

    protected $table = 'tblDevelopmentCampaignProduct';

    protected $keyType = 'string';

    public $timestamps = false;

}
