<?php

namespace App\Models\PatronBase;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = [];

    protected $table = 'tblSaleBooking';

    protected $primaryKey = 'SaleID';

    public $timestamps = false;


}
