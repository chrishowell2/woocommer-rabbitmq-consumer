<?php

namespace App\Models\PatronBase;

use Illuminate\Database\Eloquent\Model;

class DevelopmentOpportunity extends Model
{
    protected $fillable = [];

    protected $table = 'tblDevelopmentOpportunity';

    protected $keyType = 'string';

    public $timestamps = false;

}
