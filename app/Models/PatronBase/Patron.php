<?php

namespace App\Models\PatronBase;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Patron extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tblPatron';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    // protected $connection = 'sqlsrv';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'PatronID';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'CardType',
        'CardNo',
        'Expiry',
        'BankNo',
        'BranchNo',
        'Password',
    ];

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['points_total'];
    // protected $appends = [

    // ];

    /**
     * The letter code for an active Patron.
     *
     * @var string
     */
    public static $IS_ACTIVE = 'Y';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'Changed';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'Changed';

    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return 'PatronID';
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->{$this->getAuthIdentifierName()};
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->Password;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * We are returning null as we aren't using remember tokens.
     *
     * @return string|null
     */
    public function getRememberToken()
    {
        return null;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * We aren't doing anything here as we aren't using remember tokens.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {
        //
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * We are returning null as we aren't using remember tokens.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return null;
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive(Builder $query)
    {
        return $query->where('tblPatron.Active', self::$IS_ACTIVE);
    }

    /**
     * Get the patron's email address.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function patronEmail()
    {
        return $this->hasOne(PatronEmail::class, 'PatronID', 'PatronID');
    }

    /**
     * Get the patron's sales.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sales()
    {
        return $this->hasMany(Sale::class, 'PatronID', 'PatronID');
    }

    /**
     * Get the patron's town.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function town()
    {
        return $this->hasOne(Town::class, 'TownID', 'TownID');
    }

    /**
     * Get the patron's phone numbers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function phones()
    {
        return $this->hasMany(PatronPhone::class, 'PatronID', 'PatronID');
    }

    /**
     * Get the patron's PatronAttributes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attributes()
    {
        return $this->hasMany(PatronAttribute::class, 'PatronID', 'PatronID');
    }

    /**
     * Set a given attribute on the model.
     *
     * We are overriding this method so nothing happens when Laravel tries to set
     * the remember token.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();

        if (!$isRememberTokenAttribute) {
            parent::setAttribute($key, $value);
        }
    }

    /**
     * Make the EmailAddress attribute return the email from the patronEmail relation
     * rather than the EmailAddress column.
     *
     * @return string|null
     */
    public function getEmailAddressAttribute()
    {
        $relation = $this->patronEmail;

        return $relation ? $relation->Email : null;
    }

    /**
     * Get the patron's PatronPoints.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function points()
    {
        return $this->hasMany(PatronPoints::class, 'PatronID', 'PatronID');
    }

    /**
     * Get the Current PatronPoints Total without expired points.
     *
     * @return int|null
     */
    public function getPointsTotalAttribute()
    {
        return $this->points()
            ->whereNull('Expiry')
            ->orWhere('Expiry', '>=', Carbon::now())
            ->sum('Points');
    }


}
