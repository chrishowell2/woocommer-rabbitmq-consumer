<?php

namespace App\Models\PatronBase;

use Illuminate\Database\Eloquent\Model;

class Production extends Model
{
    protected $fillable = [];

    protected $table = 'tblProd';

    protected $primaryKey = 'ProdID';

    protected $keyType = 'string';

    public $timestamps = false;


}
