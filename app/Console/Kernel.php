<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use App\Console\Commands\ConsumeNewSales;
use App\Console\Commands\ConsumeFailedSales;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ConsumeNewSales::class,
        ConsumeFailedSales::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('consume:new-sales UKBeta')->everyMinute();
        $schedule->command('consume:failed-sales UKBeta')->daily();
    }
}
