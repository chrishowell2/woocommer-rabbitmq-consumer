<?php

namespace App\Console\Commands;

use App\Lib\Consumer\NewSalesConsumer;
use Illuminate\Console\Command;
use Bschmitt\Amqp\Facades\Amqp;

class ConsumeNewSales extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'consume:new-sales {connection}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Consume new sales';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $connection = $this->argument('connection');
        $className = "App\Lib\PatronBase\Connections\\$connection";

        if (class_exists($className)) {
            echo 'Getting new sales' . PHP_EOL;
            $class = new $className();

            $sales = new NewSalesConsumer($class);

            echo 'Adding ';
            echo $sales->getPayload();
            echo ' Sales';
        }

        echo PHP_EOL;
        echo 'Finish' .PHP_EOL;

    }
}
