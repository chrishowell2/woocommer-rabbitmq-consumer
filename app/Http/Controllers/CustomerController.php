<?php

namespace App\Http\Controllers;
use App\Lib\PatronBase\Connections\UKBeta;
use App\Lib\Consumer\NewPatronsConsumer;
use Carbon\Carbon;
use Bschmitt\Amqp\Facades\Amqp;

class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->connection = $connection;
        // $this->patrons = $this->handle();
    }

    public function customers()
    {
        Amqp::consume('cust-tests', function ($message, $resolver) {

            $resolver->acknowledge($message);

            $resolver->stopWhenProcessed();

        });
    }
}
