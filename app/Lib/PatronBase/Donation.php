<?php

namespace App\Lib\PatronBase;

use DB;
use Carbon\Carbon;

class Donation
{
    /**
     * Connection
     *
     * @var PatronbaseConnection $connection
     */
    public $connection;


    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    /**
     * Add donation to sale
     *
     * @param mixed $value
     * @return array
     */
    public function create()
    {

    }

}
