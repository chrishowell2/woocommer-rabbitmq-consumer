<?php

namespace App\Lib\PatronBase;

use DB;
use App\Models\PatronBase\Patron;
use App\Models\PatronBase\PatronEmail;
use Carbon\Carbon;

class NewPatron
{
    /**
     * Connection
     *
     * @var PatronbaseConnection $connection
     */
    public $connection;


    public function __construct($connection, array $details)
    {
        $this->connection = $connection;

        $this->first_name = $details['first_name'];
        $this->last_name = $details['last_name'];
        $this->email = $details['email'];
        $this->phone = $details['phone'];
        $this->address_1 = $details['address_1'];
        $this->address_2 = $details['address_2'];
        $this->city = $details['city'];
        $this->state = $details['state'];
        $this->country = $details['country'];
        $this->postcode = $details['postcode'];
        // Never have an address 3 in woocomerce
        $this->address_3 = null;
    }

    /**
     * Create A new Patron
     *
     * @param mixed $value
     * @return object
     */
    public function create()
    {
         // Perform main query to execute new patron statement
        $new_patron_sql = "SET NOCOUNT ON EXEC up_p_create_patron @firstName = '$this->first_name', @lastName = '$this->last_name', @homePhone = '$this->phone', @operatorId = 'WEB'";
        $query = DB::connection($this->connection->getDataBaseName())
            ->select(DB::connection($this->connection->getDataBaseName())
            ->raw($new_patron_sql));

        // Grab the patron ID of the new patron
        $patron = $query[0];
        $patron_id = $query[0]->PatronID;

       // Update tblPatronEmail with the supplied email address
        $patron_email = PatronEmail::on($this->connection->getDataBaseName())
            ->create([
                'PatronID'  => $patron_id,
                'Email' => $this->email,
                'Changed' => Carbon::now(),
                'ChangedBy' => 'WEB'
            ]);

        $patron_email->patron->PreferredEmail = $patron_email->ID;
        $patron_email->patron->WebMailAddress = $this->address_1;
        $patron_email->patron->WebMailAddress2 = $this->address_2;
        $patron_email->patron->WebMailAddress3 = $this->address_3;
        $patron_email->patron->WebMailState = $this->state;
        $patron_email->patron->WebMailPostCode = $this->postcode;
        $patron_email->patron->save();

        return $patron_email->patron;
    }

}
