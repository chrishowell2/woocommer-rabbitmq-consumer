<?php

namespace App\Lib\PatronBase;

use App\Models\PatronBase\Booking;
use DB;
use Carbon\Carbon;
use App\Models\PatronBase\Product as ProductModel;
use Log;

class Sale
{
    /**
     * Connection
     *
     * @var PatronbaseConnection $connection
     */
    public $connection;

    public $sale_id;

    public $sale_scheme;


    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    /**
     * Create
     *
     * @param mixed $value
     * @return array
     */
    public function create($patron_id, $customer_note)
    {
        $sale =  DB::connection($this->connection->getDataBaseName())
            ->select("SET NOCOUNT ON;
                EXEC up_saleCreate_v2
                @patronId = :patronId,
                @operatorId = :operatorId,
                @notes = :notes,
                @mailOutTickets = :mailOutTickets
            ", [
                'patronId' => $patron_id,
                'operatorId' => 'WEB',
                'notes' => $customer_note,
                'mailOutTickets' => 0
            ]);
        $this->sale_id = $sale[0]->SaleID;
    }

    /**
     * AddProduct
     *
     * @param mixed $value
     * @return array
     */
    public function AddProduct($productId, $qty, $price, $lineNum)
    {
        $add_product_sale =  DB::connection($this->connection->getDataBaseName())
            ->select("SET NOCOUNT ON;
                EXEC up_addProductToSale
                @saleID = :saleID,
                @productID = :productID,
                @quantity = :quantity,
                @unitPrice = :unitPrice,
                @paid  = :paid,
                @operatorId = :operatorId,
                @prodID = :prodID,
                @lineNum = :lineNum
            ", [
                'saleID' => $this->sale_id,
                'productID' => $productId,
                'quantity' => $qty,
                'unitPrice' => $price,
                'paid' => 'Y',
                'operatorId' => 'WEB',
                'prodID' => null,
                'lineNum' => -1
            ]);
    }

    /**
     * AddDonation
     *
     * @param mixed $value
     * @return array
     */
    public function AddDonation($productId, $qty, $price, $lineNum, $opportunityId = null, $campaignId = null, $giftAid = false )
    {
        $sale_id = $this->sale_id;
        // $sale_id = 203817;
        $add_donation_sale =  DB::connection($this->connection->getDataBaseName())
            ->statement("SET NOCOUNT ON;
                EXEC spSaleItemDonationAdd
                @saleID = :saleID,
                @productID = :productID,
                @quantity = :quantity,
                @unitPrice = :unitPrice,
                @paid  = :paid,
                @operatorId = :operatorId,
                @prodID = :prodID,
                @inventoryItemID = :inventoryItemID,
                @isUnacknowledged = :isUnacknowledged,
                @opportunityID = :opportunityID,
                @campaignID = :campaignID,
                @donationValue = :donationValue,
                @lineNum = :lineNum,
                @giftAid = :giftAid,
                @err = :err,
                @errStr = :errStr

            ", [
                'saleID' => $sale_id,
                'productID' => $productId,
                'quantity' => $qty,
                'unitPrice' => $price,
                'paid' => 'Y',
                'operatorId' => 'WEB',
                'prodID' => null,
                'inventoryItemID' => null,
                'isUnacknowledged' => 0,
                'opportunityID' => $opportunityId,
                'campaignID' => $campaignId,
                'donationValue' =>  $price,
                'lineNum' => -1,
                'giftAid' => $giftAid,
                'err' => -1,
                'errStr' => -1
            ]);

    }

    /**
     * Create
     *
     * @param mixed $value
     * @return array
     */
    public function AddPayment($total, $paymentType)
    {
        $sale =  DB::connection($this->connection->getDataBaseName())
            ->select("SET NOCOUNT ON;
                EXEC up_s_addPayment
                @saleID = :saleID,
                @operatorId = :operatorId,
                @amount = :amount,
                @paymentType = :paymentType
            ", [
                'saleID' => $this->sale_id,
                'operatorId' => 'WEB',
                'amount' => $total,
                'paymentType' => $paymentType
            ]);
        return $sale[0];
    }

    /**
     * Create Sale Scheme
     *
     * @param mixed $value
     * @return array
     */
    public function CreateSaleAgreement($patron_id, $sale_scheme, $payment_date, $payment_type)
    {
        $query = DB::connection($this->connection->getDataBaseName())
            ->select("
                SET NOCOUNT ON;
                EXEC spSaleAgreementCreate
                @patronID = :patronID,
                @recurringSaleSchemeID = :recurringSaleSchemeID,
                @name = :name,
                @startDate = :startDate,
                @endDate = :endDate,
                @firstPaymentDate = :firstPaymentDate,
                @pmtType = :pmtType,
                @notes = :notes,
                @operatorID = :operatorID,
                @tokenID = :tokenID,
                @paymentRef = :paymentRef
            ", [
                'patronID' => $patron_id,
                'recurringSaleSchemeID' => $sale_scheme->SchemeID,
                'name' => $sale_scheme->Description,
                'startDate' => $payment_date,
                'endDate' => null,
                'firstPaymentDate' => $payment_date,
                'pmtType' => $payment_type,
                'notes' => '',
                'operatorID' => 'WEB',
                'tokenID' => null,
                'paymentRef' => null
            ]);

            $this->sale_scheme = $query[0]->SaleAgreementID;


        return $query[0];
    }
    /**
     * Add to Sale Scheme
     *
     * @param mixed $value
     * @return array
     */
    public function AddProductToSaleAgreement($productId, $qty, $price, $lineNum)
    {
        $query = DB::connection($this->connection->getDataBaseName())
            ->statement("EXEC spSaleAgreementAddProduct
                @saleAgreementID = :saleAgreementID,
                @productID = :productID,
                @quantity = :quantity,
                @unitPrice = :unitPrice,
                @operatorId = :operatorId,
                @applicabilityID = :applicabilityID,
                @lineNum = :lineNum
            ", [
                'saleAgreementID' =>  $this->sale_scheme,
                'productID' => $productId,
                'quantity' => $qty,
                'unitPrice' => $price,
                'operatorId' => 'WEB',
                'applicabilityID' => 'A',
                'lineNum' => -1,
            ]);

        return $query[0];
    }
    /**
     * Add to Sale Scheme
     *
     * @param mixed $value
     * @return array
     */
    public function AddDonationToSaleAgreement($productId, $qty, $price, $lineNum, $opportunityId, $campaign_id, $gift_aid)
    {
        $query = DB::connection($this->connection->getDataBaseName())
            ->statement("EXEC spSaleAgreementItemDonationAdd
                @saleAgreementID = :saleAgreementID,
                @productID = :productID,
                @quantity = :quantity,
                @unitPrice = :unitPrice,
                @operatorId = :operatorId,
                @applicabilityID = :applicabilityID,
                @isUnacknowledged = :isUnacknowledged,
                @opportunityID = :opportunityID,
                @campaignID = :campaignID,
                @donationValue = :donationValue,
                @lineNum = :lineNum,
                @giftAid = :giftAid,
                @donorRecognitionName = :donorRecognitionName,
                @dedication = :dedication,
                @err = :err,
                @errStr = :errStr
            ", [
                'saleAgreementID' =>  $this->sale_scheme,
                'productID' => $productId,
                'quantity' => $qty,
                'unitPrice' => $price,
                'operatorId' => 'WEB',
                'applicabilityID' => 'A',
                'isUnacknowledged' => 0,
                'opportunityID' => $opportunityId,
                'campaignID' => $campaign_id,
                'donationValue' => $price,
                'lineNum' => -1,
                'giftAid' => $gift_aid,
                'donorRecognitionName' => null,
                'dedication' => null,
                'err' => -1,
                'errStr' => -1
            ]);

        return $query[0];
    }
    /**
     * Add to booking to sale
     *
     * @param mixed $value
     * @return array
     */
    public function AddBookingToSale($prodID, $perfID, $qty)
    {
        $query = DB::connection($this->connection->getDataBaseName())
            ->select("SET NOCOUNT ON;
                EXEC up_addNumSeatsToSale
                @saleID = :saleID,
                @prodID = :prodID,
                @perfID = :perfID,
                @numSeats = :numSeats,
                @operatorId = :operatorId
            ", [
                'saleID' =>  $this->sale_id,
                'prodID' => $prodID,
                'perfID' => $perfID,
                'numSeats' => (int)$qty,
                'operatorId' => 'WEB'
            ]);

        return $query[0];
    }

    /**
     * Update booktype
     *
     * @param mixed $value
     * @return array
     */
    public function UpdateBookType($bookType)
    {
        $bookings = Booking::on($this->connection->getDataBaseName())->where('SaleID', $this->sale_id)->get();

        foreach ($bookings as $booking) {
            $booking->BookTypeID = $bookType;
            $booking->save();
        }

    }

        /**
     * Find a product by sku
     *
     * @param mixed $value
     * @return object
     */
    public function findBySku($sku)
    {
        return ProductModel::on($this->connection->getDataBaseName())->find(Product::skuMap($sku));
    }
    /**
     * Find a product by patronbase_variation_id
     *
     * @param mixed $value
     * @return object
     */
    public function findByVariation($meta)
    {
        return ProductModel::on($this->connection->getDataBaseName())->find(Product::findByVariationFromMeta($meta));
    }

    /**
     * Find a product by its id
     * Mostly for shipping
     *
     * @param mixed $value
     * @return object
     */
    public function findById($meta)
    {
        return ProductModel::on($this->connection->getDataBaseName())->find($meta);
    }

    /**
     * Confirm sale
     *
     * @param mixed $value
     *
     */
    public function confirmSale($patron_id)
    {
        $query = DB::connection($this->connection->getDataBaseName())
            ->select("SET NOCOUNT ON;
                EXEC up_saleConfirm
                @saleID = :saleID,
                @patronId = :patronId,
                @shiftId = :shiftId,
                @operatorId = :operatorId,
                @referralId = :referralId,
                @sourceId = :sourceId
            ", [
                'saleID' =>  $this->sale_id,
                'patronId' => $patron_id,
                'shiftId' => NULL,
                'operatorId' => 'WEB',
                'referralId' => 7,
                'sourceId' => NULL
            ]);

        return $query[0];
    }

    /**
     * Update payment status
     *
     * @param $sale_id
     *
     */
    public function updatePaymentStatus($sale_id)
    {
        $query = "UPDATE tblSaleBooking SET Paid = :paid WHERE SaleID = :sale_id";
        $params = [
            'paid' => 'Y',
            'sale_id' => $this->sale_id
        ];

        $update = DB::connection($this->connection->getDataBaseName())
            ->update($query, $params);
    }
}
