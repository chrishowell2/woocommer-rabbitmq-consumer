<?php

namespace App\Lib\PatronBase;

use Carbon\Carbon;
use App\Models\PatronBase\PatronChannel;

class Channel
{
    /**
     * Connection
     *
     * @var PatronbaseConnection $connection
     */
    public $connection;

    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    /**
     * Update patron channel
     *
     * @param mixed $value
     * @return void
     */
    public function updatePatronChannel($patron_id, $opt_in)
    {
        $allowed = 0;
        if($opt_in == 'yes') {
            $allowed = 1;
        }

        $patron_channel = PatronChannel::on($this->connection->getDataBaseName())
                ->where('PatronID', $patron_id)
                ->where('ChannelID', 'EML')
                ->where('PrivacyLevel', 10)->first();

        $patron_channel->Allowed = $allowed;
        $patron_channel->Changed = Carbon::now();
        $patron_channel->ChangedBy = 'WEB';
        $patron_channel->ConsentSourceID = 'W';
        $patron_channel->save();

    }
}
