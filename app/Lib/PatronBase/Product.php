<?php

namespace App\Lib\PatronBase;

use App\Lib\PatronBase\Connections\PatronBaseConnection;
use App\Models\PatronBase\Production;
use DB;

class Product
{
    /**
     * Connection
     *
     * @var PatronbaseConnection $connection
     */


    public function __construct(PatronBaseConnection $connection)
    {

    }

    /**
     * Find a product by sku
     *
     * This could be from anywhere depends how they are having there skus
     * Would need a full list of what they had if we did this way,
     * better to have a either in patronbase or the skus are patronbase ids
     *
     * @param string $sku
     * @return string
     */
    public static function skuMap($sku)
    {

        $skuList = [
            "" => 'TP',
            "grove-celebration" => 'I176',
            "tfl_grove_tree" => 'I176',
            "tfl_donate_general" => 'I252',
            "e-certificate-1" => 'I172',
            "e-certificate" => 'I268',
            "tfl_certificate-tree" => 'TP',
            "tfl-donate-to-grove" => 'I176',
            "tfl_grove_tree" => 'I319',
        ];

        return $skuList[$sku];
    }

    /**
     * is volunteer
     *
     * need to split the sku to see if we have a booking /volenteer
     * Dundreggan 1st July - Standard = D001 / 1 / STAN
     * Dundreggan 1st July - Member = D001 / 1 / MEMB
     * prod id perf and membertype
     *
     * @param string $sku
     * @return string
     */
    public static function isVolunteer($sku, $connection)
    {
        $volunteer = explode('-', $sku );

        if(count($volunteer) === 3) {
            $volunteer = array_map('trim', $volunteer);
            $volunteer = [
                'ProdID' => $volunteer[0],
                'PerfID' => $volunteer[1],
                'MemType'  => $volunteer[2]
            ];

            $production = Production::on($connection->getDataBaseName())->where('ProdID', $volunteer['ProdID'])->first();
            if($production) {
                return $volunteer;
            }

        }

        return false;
    }



    /**
     * FindPaymentType
     *
     * find the payment type id form a list of mapped ids
     * @param string $sku
     * @return string
     */
    public static function paymentTypeMap($method)
    {
        $methodList = [
            "stripe" => 'M',
            "ppec_paypal" => 'V',
            "rp_smartdebit" => 'INV',
            "cheque" => 'Q'
        ];

        if(!empty($methodList[$method])) {
            return $methodList[$method];
        }

        return 'M';
    }

    /**
     * donationTypeMap
     *
     * map the donation type to patron record
     * @param string $sku
     * @return string
     */
    public static function donationTypeMap($method)
    {
        $methodList = [
            "1_month" => 'M'
        ];

        return $methodList[$method];
    }


    /**
     * Need to work out gift aid
     *
     * With this we only get the question and answer as the id isnt the same for each and is not a yes no variable
     * @param array $data meta data
     * @return bool
     */
    public static function giftAid($data)
    {
        $key = 'Would you like to apply Gift Aid to your donation?';
        $answer = "Do NOT reclaim the tax paid on my donations.";

        $is_gift_aid = array_values(array_filter($data, function($aid) use ($key) {
            return $aid['key'] == $key;
        }));

        if (count($is_gift_aid) > 0 && $is_gift_aid[0]['value'] != $answer) {
            return true;
        }

        return false;
    }

    /**
     * findGrove ID
     *
     * Need to search meta data for the grove id
     * You'll get a grove ID from Wordpress.
     * Search tblDevelopmentOpportunity for an opportunity reference which matches the grove ID,
     * then add the donation with the appropriate OpportunityID.
     *
     * @param array $data meta data
     * @return integer
     **/
    public static function findGrove($data)
    {
        $key = 'linked_posts';

        $grove = array_values(array_filter($data, function ($q) use ($key) {
            return $q['key'] == $key;
        }));

        if (!empty($grove[0]['value'])) {
            return $grove[0]['value'];
        }

        return null;
    }

    /**
     * findByVariationFromMeta
     *
     * Need to search meta data for the patronbase_variation_id
     * @param array $data meta data
     * @return integer
     **/
    public static function findByVariationFromMeta($data)
    {
        // They have eiter for some reason
        $key = 'patronbase_variation_id';

        $variation = array_values(array_filter($data, function ($q) use ($key) {
            return $q['key'] == $key;
        }));

        if (!empty($variation[0]['value'])) {
            return $variation[0]['value'];
        }

        $key = 'patronbase_id';

        $variation = array_values(array_filter($data, function ($q) use ($key) {
            return $q['key'] == $key;
        }));

        if (!empty($variation[0]['value'])) {
            return $variation[0]['value'];
        }

        return null;
    }

    /**
     * findDonationLength
     *
     * need to work out the donation length using the
     *
     * @param array $data meta data
     * @return mixed
     **/
    public static function findDonationLength($data)
    {
        $key = '_wcsatt_scheme';

        $donation = array_values(array_filter($data, function($q) use ($key) {
            return $q['key'] == $key;
        }));

        if (!empty($donation[0]['value'])) {
            return $donation[0]['value'];
        }

        return 0;
    }

    /**
     * isSubscriberDonation
     *
     * we need to work out if the sale has a subscriber donation
     *
     * @param array $data line items
     * @return bool
     **/
    public static function isSubscriberDonation($data)
    {
        $key = '_wcsatt_scheme';

        foreach ($data as $item) {
            $isDonation = array_values(array_filter($item['meta_data'], function($q) use ($key) {
                return $q['key'] == $key && $q['value'] != 0;
            }));
            if(!empty($isDonation)) {
                return true;
            }
        }
        return false;
    }

}
