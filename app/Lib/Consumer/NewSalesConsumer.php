<?php

namespace App\Lib\Consumer;

use App\Lib\PatronBase\Connections\PatronBaseConnection;
use App\Lib\PatronBase\NewPatron;
use App\Lib\PatronBase\Product;
use App\Lib\PatronBase\Sale;
use App\Lib\PatronBase\Channel;
use App\Models\PatronBase\DevelopmentCampaignProduct;
use App\Models\PatronBase\DevelopmentOpportunity;
use App\Models\PatronBase\RecurringSaleScheme;
use App\Models\PatronBase\Patron;
use App\Models\PatronBase\PatronEmail;
use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use DB;
use Log;

class NewSalesConsumer
{
    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct(PatronbaseConnection $connection)
    {
        $this->connection = $connection;
        $this->payload = null;
        $this->handle();
    }

    /**
     * Execute
     *
     * @return void
     */
    public function handle()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        Amqp::consume($this->connection->getNewSalesQueueName(), function ($message, $resolver) {
            Log::info('Checking for data');
            if (!empty($message->body)) {
                $sales = json_decode($message->body, true);
                // Use this for Products Example tests: $sales = json_decode(file_get_contents(base_path('Products Example.json')), true);
                foreach ($sales as $sale) {
                    $validated = [];
                    $log_message = null;
                    $sale_scheme = null;
                    $new_sale = new Sale($this->connection);
                    $email = $sale['billing']['email'];

                        //Do some validation
                    foreach ($sale['line_items'] as $item) {
                        $volunteer = Product::isVolunteer($item['sku'], $this->connection);
                        $product = $new_sale->findByVariation($item['meta_data']);

                        if(!$product && !$volunteer) {
                            Log::info('Missing variation id and is not volunteer');
                            $log_message = "Line item is missing a variation ID and is not a volunteer week";
                            $validated[] = false;
                        }
                    }
                    if (empty($email)) {
                        Log::info('Missing Sale Email');
                        $log_message = "Sale email address is missing";
                        $validated[] = false;
                    }

                    $is_patron = PatronEmail::where('Email', $email)
                                            ->first();

                    if (!empty($is_patron)) {
                        // Create the sale
                        $patron_id = $is_patron->PatronID;
                    } else {
                        // We dont have a patron, lets create one
                        $patron = new NewPatron($this->connection, $sale['billing']);
                        $patron = $patron->create();
                        $patron_id = $patron->PatronID;
                    }

                    // Get email content value and update patron channel
                    $opt_in = $sale['meta_data'][1];

                    $channel = new Channel($this->connection);
                    $channel->updatePatronChannel($patron_id, $opt_in['value']);

                    $payment_type = Product::paymentTypeMap($sale['payment_method']);

                    $lineNum = 1;
                    $subscription = null;

                    // Because we need to handle subscriptions differently
                    $isSubscription = Product::isSubscriberDonation($sale['line_items']);

                    // For subscriptions and more validation
                    if($isSubscription) {
                        foreach ($sale['line_items'] as $item) {
                            $donation_length = Product::findDonationLength($item['meta_data']);
                            $donation_key = Product::donationTypeMap($donation_length);
                            $sale_scheme = RecurringSaleScheme::on($this->connection->getDataBaseName())
                                ->where('RecurPeriodID', $donation_key)
                                ->first();
                            // Break out of the loop so we dont make it again
                            if($sale_scheme) {
                                break;
                            }
                        }
                        // if we dont have one we have missed some validation
                        if(!$sale_scheme) {
                            Log::info('Missing Recuring Sale Scheme');
                            $log_message = "Recurring sale scheme is missing";
                            $validated[] = false;

                        }

                    }

                    if(!empty($validated)) {
                        // We have failed validation so we need to continue and put this in the failed queue
                        Log::info('Failed validation' . json_encode($sale));

                        $payload = $sale;
                        $s_id = $sale['id'];
                        $mail_err = 'Order with ID #'.$s_id.' could not be added to PatronBase. Reason: '.$log_message;

                        Amqp::publish($this->connection->getFailedSalesRoutekey(), json_encode($payload), [
                                    'queue' => $this->connection->getFailedSalesQueueName(),
                                    'exchange' => $this->connection->getExchange()
                            ]);

                        continue;
                    }


                    $new_sale->create($patron_id, $sale['customer_note']);
                    // If we have a sale scheme we need to make an agreement
                    if($sale_scheme) {
                            $payment_date = Carbon::parse($sale['date_created'])->toDateTimeString();
                            $new_sale->CreateSaleAgreement(
                                $patron_id,
                                $sale_scheme,
                                $payment_date,
                                $payment_type
                            );
                    }


                    if (!$isSubscription) {
                        // Handle normal products and donations
                        foreach ($sale['line_items'] as $item) {
                            $volunteer = Product::isVolunteer($item['sku'], $this->connection);
                            $product = $new_sale->findByVariation($item['meta_data']);

                            Log::info($item);

                            if ($volunteer !== false) {
                                $new_sale->AddBookingToSale(
                                    $volunteer['ProdID'],
                                    $volunteer['PerfID'],
                                    $item['quantity']
                                );
                                $new_sale->UpdateBookType($volunteer['MemType'], $item['total'] + $item['total_tax']);
                                $new_sale->confirmSale($patron_id);
                                $new_sale->updatePaymentStatus($new_sale->sale_id);
                            } else if ($product && $product->ProductType == 'P') {
                                // up_addProductToSale
                                $new_sale->AddProduct($product->ProductID, $item['quantity'], $item['total'] + $item['total_tax'], $lineNum);

                            } else if($product) {
                                // Work out if we have any campaign products
                                $campaign_products = DevelopmentCampaignProduct::on($this->connection->getDataBaseName())->where('ProductID', $product->ProductID)->get();
                                $campaign_id = null;
                                //If the product has 0 or 2+ campaigns, don't set the campaign ID.
                                if ($campaign_products && $campaign_products->count() === 1) {
                                    $campaign_id == $campaign_products->first()->CampaignID;
                                }
                                // Workout gift aid
                                $gift_aid = Product::giftAid($item['meta_data']);
                                $grove_id = Product::findGrove($sale['meta_data']);
                                $opportunity = DevelopmentOpportunity::on($this->connection->getDataBaseName())->where('OpportunityReference', $grove_id)->first();
                                $opportunity_id = $opportunity->OpportunityID ?? null;

                                $new_sale->AddDonation(
                                    $product->ProductID,
                                    $item['quantity'],
                                    $item['total'] + $item['total_tax'],
                                    $lineNum,
                                    $opportunity_id,
                                    $campaign_id,
                                    $gift_aid
                                );
                            }

                            $lineNum++;
                        }
                    } else {

                        foreach ($sale['line_items'] as $item) {

                            if (is_null($subscription)) {

                                $product = $new_sale->findByVariation($item['meta_data']);
                                // Work out if we have any campaign products
                                $campaign_products = DevelopmentCampaignProduct::on($this->connection->getDataBaseName())->where('ProductID', $product->ProductID)->get();
                                $campaign_id = null;
                                //If the product has 0 or 2+ campaigns, don't set the campaign ID.
                                if ($campaign_products && $campaign_products->count() === 1) {
                                    $campaign_id == $campaign_products->first()->CampaignID;
                                }
                                // Workout gift aid
                                $gift_aid = Product::giftAid($item['meta_data']);
                                $grove_id = Product::findGrove($sale['meta_data']);
                                $opportunity = DevelopmentOpportunity::on($this->connection->getDataBaseName())->where('OpportunityReference', $grove_id)->first();

                                if ($product->ProductType == 'P') {
                                    $new_sale->AddProductToSaleAgreement(
                                        $product->ProductID,
                                        $item['quantity'],
                                        $item['total'] + $item['total_tax'],
                                        $lineNum
                                    );
                                } else {
                                    $new_sale->AddDonationToSaleAgreement(
                                        $product->ProductID,
                                        $item['quantity'],
                                        $item['total'] + $item['total_tax'],
                                        $lineNum,
                                        $opportunity->OpportunityID ?? null,
                                        $campaign_id,
                                        $gift_aid
                                    );
                                }
                            }
                        }
                    }
                    // ADD SHIPPING
                    if (!empty($sale['shipping_lines'])) {
                        $shipping = $sale['shipping_lines'][0];
                        // Hard coded
                        $shippingProduct = $new_sale->findById('PF');

                        if ($shippingProduct) {
                            $new_sale->AddProduct($shippingProduct->ProductID, 1, $shipping['total'] + $shipping['total_tax'], -1);
                        } else {
                            Log::info('Missing Recuring Shipping Product' . json_encode($sale));
                        }
                    }

                    $payment = $new_sale->AddPayment($sale['total'], $payment_type);

                    $confirm = $new_sale->confirmSale($new_sale->sale_id, $patron_id);

                    Log::info('Completed sale' .  $new_sale->sale_id);

                }
            }

            $resolver->acknowledge($message);
            $resolver->stopWhenProcessed();
        });
        echo 'finish' .PHP_EOL;
    }

    public function getPayload()
    {
        return $this->payload;
    }
}
