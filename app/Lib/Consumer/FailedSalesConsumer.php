<?php

namespace App\Lib\Consumer;

use App\Lib\PatronBase\Connections\PatronBaseConnection;
use Bschmitt\Amqp\Facades\Amqp;
use Log;

class FailedSalesConsumer
{
    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct(PatronbaseConnection $connection)
    {
        $this->connection = $connection;
        $this->payload = null;
        $this->handle();
    }

    /**
     * Execute the patron Stuff
     *
     * @return void
     */
    public function handle()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        Amqp::consume($this->connection->getFailedSalesQueueName(), function ($message, $resolver) {
            Log::info('Checking for data');
            if (!empty($message->body)) {
                $sales = json_decode($message->body, true);
                $payload = [$sales];

                Amqp::publish($this->connection->getNewSalesRoutekey(), json_encode($payload), [
                    'queue' => $this->connection->getNewSalesQueueName(),
                    'exchange' => $this->connection->getExchange()
                ]);
            }
            $resolver->acknowledge($message);
            $resolver->stopWhenProcessed();
        });

        echo 'finish' .PHP_EOL;
    }

    public function getPayload()
    {
        return $this->payload;
    }
}
